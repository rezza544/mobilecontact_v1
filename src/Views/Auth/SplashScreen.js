import React from 'react';
import {
    View, Text, Image
} from 'react-native';
import StatusBarDefault from '../../Components/StatusBar/StatusBarDefault';

import { Color } from '../../Utils/Colors/Color';

class SplashScreen extends React.Component {
    async UNSAFE_componentWillMount() {
        this.handleCheckSplashScreen();
    }

    handleCheckSplashScreen = async() => {
        try {
            setTimeout(() => {
                this.props.navigation.navigate('AppNavigator');
            }, 500)
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: Color.Hijau
                }}
            >
                <StatusBarDefault 
                    backgroundColor={Color.Hijau}
                />

                <Image 
                    source={require('../../Images/Icon/icon_book.png')}
                />

                <Text
                    style={{
                        color: Color.Putih,
                        fontSize: 20,
                        fontWeight: "bold",
                        letterSpacing: 1
                    }}
                >
                    CONTACT
                </Text>
            </View>
        )
    }
}

export default SplashScreen;