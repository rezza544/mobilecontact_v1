import React from 'react';
import {
    View, Text, Image, Platform, PermissionsAndroid
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import Header from '../../Components/Header/BasicHeader';
import StatusBarDefault from '../../Components/StatusBar/StatusBarDefault';
import ButtonIcon from '../../Components/Button/ButtonIcon';
import TextInputDefault from '../../Components/TextInput/TextInputDetaul';
import ButtonDefault from '../../Components/Button/ButtonDefault';
import ModalBottom from '../../Components/Modal/ModalBottom';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';


import { GETCONTACT } from '../../Services/Service';
import { Color } from '../../Utils/Colors/Color';

const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}
class CreateContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idUser: this.props.navigation.getParam('idUser'),

            photo: '',
            firstname: '',
            lastname: '',
            age: '',

            dataImage: '',
            menupicture: false,

            /**
             * BackgroundLine Error
             */
            colorBottomLineFirstName: false,
            colorBottomLineLastName: false,
            colorBottomLineAge: false
        }
    }

    componentDidMount() {
        this.requestPermissionCamera();
        
    }

    requestPermissionCamera = async() => {
        if(Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission'
                    }
                );

                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.log(err);
                return false;
            }
        }else {
            return false;
        }
    }

    handleMenuPicture = () => {
        this.setState({
            menupicture: !this.state.menupicture
        });
    }

    handleChangePhoto = async() => {
        let options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500,
            quality: 0.5
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = `data:${response.type};base64,${response.data}`;
                let image = response.path;

                console.log(source);

                this.setState({
                    photo: source,
                    // photo: `file:///${image}`,
                    menupicture: false
                });
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            }
        });
    }

    handleTakePicture = async() => {6
        let options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500,
            quality: 0.5
        }

        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = `data:${response.type};base64,${response.data}`;
                let image = response.path;

                console.log(source);

                this.setState({
                    photo: source,
                    // photo: `file:///${image}`,
                    menupicture: false
                });
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            }
        });
    }

    handleUpdateContact = async() => {
        try {
            const body = {
                firstName: this.state.firstname,
                lastName: this.state.lastname,
                age: this.state.age,
                photo: this.state.photo            
            }

            console.log(body);

            const putUrl = await fetch(`${GETCONTACT}/${this.state.idUser}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'Application/json'
                },
                body: JSON.stringify({
                    firstName: this.state.firstname,
                    lastName: this.state.lastname,
                    age: this.state.age,
                    photo: this.state.photo    
                })
            });

            const resJson = await putUrl.json();

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                this.props.navigation.navigate('ListContact');
            }else {
                console.log(resJson);
                alert(resJson.message);
            }

            // const putUrl = await axios(`${GETCONTACT}/${this.state.idUser}`, body,
            // {headers: headers});
            // console.log(putUrl);

            // if(putUrl.status === 200) {
            //     this.props.navigation.navigate('ListContact');
            // }else {
            //     alert(putUrl);
            //     console.log(putUrl);
            // }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputFirstName = async(value) => {
        try {
            this.setState({ firstname: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineFirstName: false
                });
            }else {
                this.setState({
                    colorBottomLineFirstName: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputLastName = async(value) => {
        try {
            this.setState({ lastname: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineLastName: false
                });
            }else {
                this.setState({
                    colorBottomLineLastName: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputAge = async(value) => {
        try {
            this.setState({ age: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineAge: false
                });
            }else {
                this.setState({
                    colorBottomLineAge: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleCreateContact = async() => {
        try {
            console.log(JSON.stringify({
                firstName: this.state.firstname,
                lastName: this.state.lastname,
                age: this.state.age,
                photo: this.state.photo   
            }));

            if(this.state.photo === '' || this.state.firstname.toString().length <= 0 || 
            this.state.lastname.toString().length <= 0 || this.state.age.toString().length <= 0) {
                this.setState({
                    colorBottomLineFirstName: true,
                    colorBottomLineLastName: true,
                    colorBottomLineAge: true
                });

                return alert('Sorry, input fields and photo cannot be empty')
            }

            const postUrl = await fetch(`${GETCONTACT}`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify({
                    firstName: this.state.firstname,
                    lastName: this.state.lastname,
                    age: this.state.age,
                    photo: this.state.photo   
                })
            });

            const resJson = await postUrl.json();

            console.log(resJson);

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                this.props.navigation.navigate('ListContact');
            }else {
                alert(resJson.message);
                console.log(resJson);
            }
        } catch (err) {
            console.log(err);
        }
    }


    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                <StatusBarDefault 
                    backgroundColor={Color.Hijau2}
                />
                <Header 
                    onPress={() => this.props.navigation.navigate('ListContact')}
                    backIsActive={true}
                    deleteIsActive={false}
                    title={'Create Contact'}
                />

                <ScrollView
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{
                            paddingVertical: 15,
                            paddingHorizontal: 20
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                            }}
                        >

                            <View
                                style={{
                                    width: 100,
                                    height: 100,
                                }}
                            >
                                <Image 
                                    style={{
                                        width: '100%', 
                                        height: '100%',
                                        resizeMode: 'cover',
                                        borderRadius: 1000
                                    }}
                                    source={this.state.photo === '' ? require('../../Images/Icon/icon_default_photo.png') : { uri: this.state.photo }}
                                />

                                <View
                                    style={{
                                        position: 'absolute',
                                        bottom: -5,
                                        right: -5,
                                    }}
                                    
                                >
                                    <ButtonIcon 
                                        onPress={() => this.handleMenuPicture()}
                                        source={require('../../Images/IconHijau/icon_pencil.png')}
                                    />
                                </View>
                            </View>
                            
                        </View>

                        <View
                            style={{
                                marginTop: 10
                            }}
                        >
                            <TextInputDefault 
                                title={'First Name'}
                                keyboardType={'default'}
                                defaultValue={this.state.firstname}
                                onChangeText={(value) => this.handleTextInputFirstName(value)}
                                borderBottomColor={this.state.colorBottomLineFirstName === true ? Color.Merah : Color.Hitam}
                            />

                            <TextInputDefault 
                                title={'Last Name'}
                                keyboardType={'default'}
                                defaultValue={this.state.lastname}
                                onChangeText={(value) => this.handleTextInputLastName(value)}
                                borderBottomColor={this.state.colorBottomLineLastName === true ? Color.Merah : Color.Hitam}
                            />

                            <TextInputDefault 
                                title={'Age'}
                                keyboardType={'numeric'}
                                defaultValue={this.state.age.toString()}
                                onChangeText={(value) => this.handleTextInputAge(value)}
                                borderBottomColor={this.state.colorBottomLineAge === true ? Color.Merah : Color.Hitam}
                            />
                        </View>

                        <View
                            style={{
                                marginTop: 20
                            }}
                        >
                            <ButtonDefault 
                                onPress={this.handleCreateContact}
                                borderRadius={40}
                                backgroundColor={Color.Hijau}
                                title={'SIMPAN'}
                            />
                        </View>

                    </View>
                </ScrollView>

                <ModalBottom 
                    isActive={this.state.menupicture}
                    takepicture={this.handleTakePicture}
                    choosegallery={this.handleChangePhoto}
                    close={this.handleMenuPicture}
                />
            </View>
        )
    }
}

export default CreateContact;
