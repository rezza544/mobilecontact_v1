import React from 'react';
import {
    View, Text, Image, Platform, PermissionsAndroid
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import Header from '../../Components/Header/BasicHeader';
import StatusBarDefault from '../../Components/StatusBar/StatusBarDefault';
import ButtonIcon from '../../Components/Button/ButtonIcon';
import TextInputDefault from '../../Components/TextInput/TextInputDetaul';
import ButtonDefault from '../../Components/Button/ButtonDefault';
import ModalBottom from '../../Components/Modal/ModalBottom';
import axios from 'axios';
import ImagePicker from 'react-native-image-picker';


import { GETCONTACT } from '../../Services/Service';
import { Color } from '../../Utils/Colors/Color';
import { connect } from 'react-redux';

const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}
class DetailContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            idUser: this.props.navigation.getParam('idUser'),

            photo: this.props.navigation.getParam('photo'),
            firstname: this.props.navigation.getParam('firstname'),
            lastname: this.props.navigation.getParam('lastname'),
            age: this.props.navigation.getParam('age'),

            dataImage: '',
            menupicture: false,

            /**
             * BackgroundLine Error
             */
            colorBottomLineFirstName: false,
            colorBottomLineLastName: false,
            colorBottomLineAge: false
        }
    }

    componentDidMount() {
        console.log(this.state.idUser);
        // this.handleGetDetailContact();
        this.requestPermissionCamera();
        console.log(this.props.age);
        
    }


    // handleGetDetailContact = async() => {
    //     try {
    //         const getUrl = await axios.get(`${GETCONTACT}/${this.state.idUser}`);

    //         if(getUrl.status === 200) {
    //             this.setState({
    //                 firstname: getUrl.data.data.firstName,
    //                 lastname: getUrl.data.data.lastName,
    //                 age: getUrl.data.data.age,
    //                 photo: getUrl.data.data.photo
    //             })
    //         }else {
    //             alert('Maaf, Ada Sebuah Kesalahan!');
    //             console.log(getUrl);
    //         }
    //     } catch (err) {
    //         console.log(err);
    //     }
    // }

    requestPermissionCamera = async() => {
        if(Platform.OS === 'android') {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'Camera Permission',
                        message: 'App needs camera permission'
                    }
                );

                return granted === PermissionsAndroid.RESULTS.GRANTED;
            } catch (err) {
                console.log(err);
                return false;
            }
        }else {
            return false;
        }
    }

    handleMenuPicture = () => {
        this.setState({
            menupicture: !this.state.menupicture
        });
    }

    handleChangePhoto = async() => {
        let options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500,
            quality: 0.5
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = `data:${response.type};base64,${response.data}`;
                let image = response.path;

                console.log(source);

                this.setState({
                    photo: source,
                    // photo: `file:///${image}`,
                    menupicture: false
                });
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            }
        });
    }

    handleTakePicture = async() => {
        let options = {
            title: 'Select Image',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
            maxWidth: 500,
            maxHeight: 500,
            quality: 0.5
        }

        ImagePicker.launchCamera(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = `data:${response.type};base64,${response.data}`;
                let image = response.path;

                console.log(source);

                this.setState({
                    photo: source,
                    // photo: `file:///${image}`,
                    menupicture: false
                });
            
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };
            
            }
        });
    }

    handleTextInputFirstName = async(value) => {
        try {
            this.setState({ firstname: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineFirstName: false
                });
            }else {
                this.setState({
                    colorBottomLineFirstName: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputLastName = async(value) => {
        try {
            this.setState({ lastname: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineLastName: false
                });
            }else {
                this.setState({
                    colorBottomLineLastName: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleTextInputAge = async(value) => {
        try {
            this.setState({ age: value });
            if(value.toString().length > 0) {
                this.setState({
                    colorBottomLineAge: false
                });
            }else {
                this.setState({
                    colorBottomLineAge: true
                });
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleUpdateContact = async() => {
        try {
            const body = {
                firstName: this.state.firstname,
                lastName: this.state.lastname,
                age: this.state.age,
                photo: this.state.photo            
            }

            console.log(body);

            

            if(this.state.photo === '' || this.state.firstname.toString().length <= 0 || 
            this.state.lastname.toString().length <= 0 || this.state.age.toString().length <= 0) {
                this.setState({
                    colorBottomLineFirstName: true,
                    colorBottomLineLastName: true,
                    colorBottomLineAge: true
                });

                return alert('Sorry, input fields and photo cannot be empty');
            }

            const putUrl = await fetch(`${GETCONTACT}/${this.state.idUser}`, {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'Application/json'
                },
                body: JSON.stringify({
                    firstName: this.state.firstname,
                    lastName: this.state.lastname,
                    age: this.state.age,
                    photo: this.state.photo    
                })
            });

            const resJson = await putUrl.json();

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                this.props.navigation.navigate('ListContact');
            }else {
                console.log(resJson);
                alert(resJson.message);
            }

            // const putUrl = await axios(`${GETCONTACT}/${this.state.idUser}`, body,
            // {headers: headers});
            // console.log(putUrl);

            // if(putUrl.status === 200) {
            //     this.props.navigation.navigate('ListContact');
            // }else {
            //     alert(putUrl);
            //     console.log(putUrl);
            // }
        } catch (err) {
            console.log(err);
        }
    }

    handleDeleteContact = async() => {
        try {
            const deleteUrl = await fetch(`${GETCONTACT}/${this.state.idUser}`, {
                method: "DELETE",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            });

            const resJson = await deleteUrl.json();

            if(resJson.statusCode === undefined || resJson.statusCode === "") {
                this.props.navigation.navigate('ListContact');
            }else {
                console.log(resJson);
                alert(resJson.message);
            }

        } catch (err) {
            console.log(err);
        }
    }


    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                <StatusBarDefault 
                    backgroundColor={Color.Hijau2}
                />
                <Header 
                    onPress={() => this.props.navigation.navigate('ListContact')}
                    onPressDelete={() => this.handleDeleteContact()}
                    deleteIsActive={true}
                    backIsActive={true}
                    title={'Detail Contact'}
                />

                <ScrollView
                    keyboardShouldPersistTaps='handled'
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{
                            paddingVertical: 15,
                            paddingHorizontal: 20
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'center',
                            }}
                        >

                            <View
                                style={{
                                    width: 100,
                                    height: 100,
                                }}
                            >
                                <Image 
                                    style={{
                                        width: '100%', 
                                        height: '100%',
                                        resizeMode: 'cover',
                                        borderRadius: 1000
                                    }}
                                    source={this.state.photo === '' || this.state.photo === 'N/A' ? require('../../Images/Icon/icon_default_photo.png') : { uri: this.state.photo }}
                                />

                                <View
                                    style={{
                                        position: 'absolute',
                                        bottom: -5,
                                        right: -5,
                                    }}
                                    
                                >
                                    <ButtonIcon 
                                        onPress={() => this.handleMenuPicture()}
                                        source={require('../../Images/IconHijau/icon_pencil.png')}
                                    />
                                </View>
                            </View>
                            
                        </View>

                        <View
                            style={{
                                marginTop: 10
                            }}
                        >
                            <TextInputDefault 
                                title={'First Name'}
                                keyboardType={'default'}
                                defaultValue={this.props.firstName}
                                onChangeText={(value) => this.handleTextInputFirstName(value)}
                                borderBottomColor={this.state.colorBottomLineFirstName === true ? Color.Merah : Color.Hitam}
                            />

                            <TextInputDefault 
                                title={'Last Name'}
                                keyboardType={'default'}
                                defaultValue={this.props.lastName}
                                onChangeText={(value) => this.handleTextInputLastName(value)}
                                borderBottomColor={this.state.colorBottomLineLastName === true ? Color.Merah : Color.Hitam}
                            />

                            <TextInputDefault 
                                title={'Age'}
                                keyboardType={'numeric'}
                                defaultValue={this.props.age.toString()}
                                onChangeText={(value) => this.handleTextInputAge(value)}
                                borderBottomColor={this.state.colorBottomLineAge === true ? Color.Merah : Color.Hitam}
                            />
                        </View>

                        <View
                            style={{
                                marginTop: 20
                            }}
                        >
                            <ButtonDefault 
                                onPress={this.handleUpdateContact}
                                borderRadius={40}
                                backgroundColor={Color.Hijau}
                                title={'UPDATE'}
                            />
                        </View>

                    </View>
                </ScrollView>

                <ModalBottom 
                    isActive={this.state.menupicture}
                    takepicture={this.handleTakePicture}
                    choosegallery={this.handleChangePhoto}
                    close={this.handleMenuPicture}
                />
            </View>
        )
    }
}

const mapStateTopProps = state => ({
    /**
     * PROFILE USERS
     */
    firstName: state.firstName,
    lastName: state.lastName,
    age: state.age,
    photo: state.photo


});

export default connect(mapStateTopProps)(DetailContact);
