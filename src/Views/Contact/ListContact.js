import React from 'react';
import {
    View, Text, ScrollView, Image, RefreshControl
} from 'react-native';

import BasicHeader from '../../Components/Header/BasicHeader';
import StatusBarDefault from '../../Components/StatusBar/StatusBarDefault';
import ListView from '../../Components/ListView/ListView';
import ButtonFloating from '../../Components/Button/ButtonFloating';
import axios from 'axios';

import { GETCONTACT } from '../../Services/Service';
import { Color } from '../../Utils/Colors/Color';
import { connect } from 'react-redux';

class ListContact extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataContact: [],


            /**
             * state Refreshing Data
             */
            refreshing: false,

        }
    }

    componentDidMount() {
        this.handleGetDataContact();

        this.didFocusListener = this.props.navigation.addListener(
            'didFocus',
            () => { this.handleGetDataContact() },
        );
    }

    
    componentWillUnmount() {
        this.didFocusListener.remove();
    }

    _onRefresh = () => {

        setTimeout(() => {
            this.handleGetDataContact();
        }, 2000);
    }


    handleGetDataContact = async() => {
        try {
            const getUrl = await axios.get(GETCONTACT);
            
            if(getUrl.status === 200) {
                this.setState({
                    dataContact: getUrl.data.data
                });
            }else {
                alert('Maaf, Ada sebuah kesalahan!');
                console.log(getUrl);
            }
        } catch (err) {
            console.log(err);
        }
    }

    handleNavigateDetail = async(value) => {
        try {
            console.log(value);
            this.props.handleSaveProfileUsers(value.firstName, value.lastName, value.age, value.photo);
            this.props.navigation.navigate('DetailContact', {
                idUser: value.id,
                photo: value.photo,
                firstname: value.firstName,
                lastname: value.lastName,
                age: value.age,

            });
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <View
                style={{
                    flex: 1
                }}
            >
                <StatusBarDefault 
                    backgroundColor={Color.Hijau2}
                />

                <BasicHeader 
                    backIsActive={false}
                    title={'Contact'}
                />

                
                <ScrollView
                    keyboardShouldPersistTaps={'handled'}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl 
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh.bind(this)}
                        />
                    }
                >
                    <View
                        style={{
                            paddingVertical: 10
                        }}
                    >
                        {
                            this.state.dataContact.map((value, key) => (
                                <ListView 
                                    key={key}
                                    onPress={() => this.handleNavigateDetail(value)}
                                    name={value.firstName}
                                    source={value.photo === 'N/A' || value.photo === '' ? require('../../Images/Icon/icon_default_photo.png') : { uri: value.photo }}
                                />
                            ))
                        }

                    </View>
                </ScrollView>

                
                <ButtonFloating 
                    onPress={() => this.props.navigation.navigate('CreateContact')}
                />
            </View>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    handleSaveProfileUsers: (firstName, lastName, age, photo) => dispatch({ type: 'PROFILE_USERS', firstName: firstName, lastName: lastName, age: age, photo: photo })
})

export default connect(null, mapDispatchToProps)(ListContact);