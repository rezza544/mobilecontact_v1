const BASEURL = `https://simple-contact-crud.herokuapp.com`;

/**
 * GET DATA CONTACT
 * ================================================
 */
export const GETCONTACT = `${BASEURL}/contact`;