import React from 'react';
import {
    View, Text, TextInput
} from 'react-native';
import { Color } from '../../Utils/Colors/Color';

class TextInputDetaul extends React.Component {
    render() {
        return (
            <View
                style={{
                    marginTop: 10
                }}
            >
                <Text
                    style={{
                        
                    }}
                >
                    {this.props.title}
                </Text>

                <View
                    style={{
                        borderBottomColor: this.props.borderBottomColor,
                        borderBottomWidth: 1,
                    }}
                >
                    <TextInput 
                        style={{
                            paddingLeft: 0,
                            paddingBottom: 2,
                            fontSize: 18
                        }}
                        onChangeText={this.props.onChangeText}
                        defaultValue={this.props.defaultValue}
                        placeholder={this.props.placeholder}
                        keyboardType={this.props.keyboardType}
                    />

                </View>
            </View>
        )
    }
}

export default TextInputDetaul;