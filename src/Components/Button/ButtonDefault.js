import React from 'react';
import {
    View,  Text, Image, TouchableNativeFeedback, TouchableOpacity, Platform
} from 'react-native';

import { Color } from '../../Utils/Colors/Color';


class ButtonIcon extends React.Component {
    render() {
        return (
            <View
                style={{
                    
                }}
            
            >

                {
                    Platform.OS === 'android' ?
                        <View
                            style={{
                                borderRadius: this.props.borderRadius,
                                paddingHorizontal: 0,
                                paddingVertical: 6,
                                backgroundColor: this.props.backgroundColor,
                                marginTop: 15
                            }}
                        >
                            <TouchableNativeFeedback
                                onPress={this.props.onPress}
                                background={TouchableNativeFeedback.Ripple(Color.Color_Ripple, true)}
                            >
                                <View
                                    style={{
                                        borderRadius: this.props.borderRadius,
                                        paddingHorizontal: 0,
                                        paddingVertical: 6,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: Color.Putih,
                                            fontSize: 16,
                                            fontWeight: 'bold',
                                            letterSpacing: 0.6
                                        }}
                                    >
                                        {this.props.title}
                                    </Text>
                                </View>
                            </TouchableNativeFeedback>

                        </View>
                    :

                        <TouchableOpacity
                            onPress={this.props.onPress}
                            style={{
                                padding: 10,
                                backgroundColor: this.props.backgroundColor,
                                borderRadius: this.props.borderRadius,
                            }}
                        >
                            
                            <Text>{this.props.title}</Text>
                        </TouchableOpacity>
                }
            </View>
        )
    }
}

export default ButtonIcon;