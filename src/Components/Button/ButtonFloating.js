import React from 'react';
import {
    View,  Text, Image, TouchableNativeFeedback, TouchableOpacity, Platform
} from 'react-native';

import { Color } from '../../Utils/Colors/Color';


class ButtonFloating extends React.Component {
    render() {
        return (
            <View
                style={{
                    position: 'absolute',
                    right: 20,
                    bottom: 20,
                }}
            
            >

                {
                    Platform.OS === 'android' ?
                        <View
                            style={{
                                borderRadius: 10000,
                                padding: 8,
                                backgroundColor: Color.Putih,
                                elevation: 5
                            }}
                        >
                            <TouchableNativeFeedback
                                onPress={this.props.onPress}
                                background={TouchableNativeFeedback.Ripple(Color.Color_Ripple, true)}
                            >
                                <View
                                    style={{
                                        borderRadius: 10000,
                                        padding: 8,
                                    }}
                                >
                                    <Image 
                                        style={{
                                            width: 30,
                                            height: 30,
                                            resizeMode: 'contain'
                                        }}
                                        source={require('../../Images/IconHijau/icon_add.png')}
                                    />
                                </View>
                            </TouchableNativeFeedback>

                        </View>
                    :

                        <TouchableOpacity
                            onPress={this.props.onPress}
                            style={{
                                padding: 15,
                                backgroundColor: Color.Putih,
                                elevation: 6,
                                borderRadius: 10000,
                                zIndex: 9999999
                            }}
                        >
                            <Image 
                                style={{
                                    width: 30,
                                    height: 30
                                }}
                                source={require('../../Images/IconHijau/icon_add.png')}
                            />
                        </TouchableOpacity>
                }
            </View>
        )
    }
}

export default ButtonFloating;