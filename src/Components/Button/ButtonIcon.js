import React from 'react';
import {
    View,  Text, Image, TouchableNativeFeedback, TouchableOpacity, Platform
} from 'react-native';

import { Color } from '../../Utils/Colors/Color';


class ButtonIcon extends React.Component {
    render() {
        return (
            <View
                style={{
                    
                }}
            
            >

                {
                    Platform.OS === 'android' ?
                        <View
                            style={{
                                borderRadius: 10000,
                                padding: 4,
                                backgroundColor: Color.Putih,
                                elevation: 5
                            }}
                        >
                            <TouchableNativeFeedback
                                onPress={this.props.onPress}
                                background={TouchableNativeFeedback.Ripple(Color.Color_Ripple, true)}
                            >
                                <View
                                    style={{
                                        borderRadius: 10000,
                                        padding: 4,
                                    }}
                                >
                                    <Image 
                                        style={{
                                            width: 24,
                                            height: 24,
                                            resizeMode: 'contain'
                                        }}
                                        source={this.props.source}
                                    />
                                </View>
                            </TouchableNativeFeedback>

                        </View>
                    :

                        <TouchableOpacity
                            onPress={this.props.onPress}
                            style={{
                                padding: 10,
                                backgroundColor: Color.Putih,
                                elevation: 6,
                                borderRadius: 10000,
                                zIndex: 9999999
                            }}
                        >
                            <Image 
                                style={{
                                    width: 20,
                                    height: 20
                                }}
                                source={this.props.source}
                            />
                        </TouchableOpacity>
                }
            </View>
        )
    }
}

export default ButtonIcon;