import React from 'react';
import {
    View, Text, Image, TouchableOpacity, TouchableNativeFeedback, Platform
} from 'react-native'

class ListView extends React.Component {
    render() {
        return (
            <View>
                {
                    Platform.OS === 'android' ?
                        <View
                            style={{
                                overflow: 'hidden'
                            }}
                        >
                            <TouchableNativeFeedback
                                onPress={this.props.onPress}
                            >
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        marginVertical: 5,
                                        paddingHorizontal: 20
                                    }}
                                >
                                    <View
                                        style={{
                                            width: 60,
                                            height: 60,
                                            borderRadius: 10000,
                                            marginRight: 15
                                        }}
                                    >
                                        <Image 
                                            style={{
                                                width: '100%',
                                                height: '100%',
                                                borderRadius: 10000,
                                                resizeMode: 'cover'
                                            }}
                                            source={this.props.source}
                                        />
                                    </View>

                                    <Text
                                        style={{
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            letterSpacing: 0.6
                                        }}
                                    >
                                        {this.props.name}
                                    </Text>
                                </View>
                            </TouchableNativeFeedback>

                        </View>

                    :
                    
                        <TouchableOpacity
                            onPress={this.props.onPress}
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginVertical: 5,
                                paddingHorizontal: 20
                            }}
                        >
                            <View
                                style={{
                                    width: 80,
                                    height: 80,
                                    borderRadius: 10000,
                                    marginRight: 15
                                }}
                            >
                                <Image 
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        borderRadius: 10000,
                                        resizeMode: 'cover'
                                    }}
                                    source={require('../../Images/Dummy/nancy.jpg')}
                                />
                            </View>

                            <Text
                                style={{
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                    letterSpacing: 0.6
                                }}
                            >
                                Reza Ahmad Fauzi
                            </Text>
                        </TouchableOpacity>
                }
            </View>
            
        )
    }
}

export default ListView;