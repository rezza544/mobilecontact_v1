import React from 'react';
import {
    StatusBar
} from 'react-native';

class StatusBarDefault extends React.Component {
    render() {
        return (
            <StatusBar 
                barStyle='light-content'
                backgroundColor={this.props.backgroundColor}
            />
        )
    }
}

export default StatusBarDefault;