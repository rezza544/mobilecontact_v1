import React from 'react';
import {
    View, Text, Image, TouchableOpacity, TouchableNativeFeedback, Platform, StyleSheet
} from 'react-native';
import { Color } from '../../Utils/Colors/Color';

class BasicHeader extends React.Component {
    render() {
        return (
            <View
                style={{
                    height: 55,
                    backgroundColor: '#388E3C',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between'
                }}
            > 

                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingLeft: 20,
                        paddingRight: 10
                    }}
                >
                    <View
                        style={this.props.backIsActive ? styles.backIsActive : styles.backIsDisabled}
                    >
                        {
                            Platform.OS === 'android' ?
                                <View
                                    style={{
                                        borderRadius: 100000,
                                        overflow: 'hidden',
                                        marginRight: 15
                                    }}
                                >
                                    <TouchableNativeFeedback
                                        onPress={this.props.onPress}
                                    >
                                        <View
                                            style={{
                                                ...this.props,
                                                borderRadius: 10000,
                                                justifyContent: 'center',
                                                alignItems: 'center'
                                            }}
                                        >
                                            <Image 
                                                style={{
                                                    width: 35,
                                                    height: 35,
                                                }}
                                                source={require('../../Images/IconPutih/icon_arrow_left.png')}
                                            />
                                        </View>
                                    </TouchableNativeFeedback>
                                </View>
                            :
                            <TouchableOpacity
                                onPress={this.props.onPress}
                                style={{
                                    marginRight: 15
                                }}
                            >
                                <Image 
                                    style={{
                                        width: 35,
                                        height: 35
                                    }}
                                    source={require('../../Images/IconPutih/icon_arrow_left.png')}
                                />
                            </TouchableOpacity>

                        }
                    </View>

                    <Text
                        style={{
                            color: Color.Putih,
                            fontWeight: 'bold',
                            fontSize: 18,
                            letterSpacing: 0.5
                        }}
                    >
                        {this.props.title}
                    </Text>
                </View>

                <View
                    style={this.props.deleteIsActive ? styles.deleteIsActive : styles.deleteDisable}
                >
                    {
                        Platform.OS === 'android' ?
                            <View
                                style={{
                                    borderRadius: 100000,
                                    overflow: 'hidden',
                                }}
                            >
                                <TouchableNativeFeedback
                                    onPress={this.props.onPressDelete}
                                >
                                    <View
                                        style={{
                                            ...this.props,
                                            borderRadius: 10000,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Image 
                                            style={{
                                                width: 25,
                                                height: 25,
                                                tintColor: Color.Putih
                                            }}
                                            source={require('../../Images/IconMerah/icon_delete.png')}
                                        />
                                    </View>
                                </TouchableNativeFeedback>
                            </View>
                        :
                        <TouchableOpacity
                            onPress={this.props.onPressDelete}
                            style={{
                            }}
                        >
                            <Image 
                                style={{
                                    width: 25,
                                    height: 25,
                                    tintColor: Color.Putih
                                }}
                                source={require('../../Images/IconMerah/icon_delete.png')}
                            />
                        </TouchableOpacity>

                    }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    deleteDisable: {
        display: 'none'
    },
    deleteIsActive: {
        paddingRight: 20,
        paddingLeft: 10
    },
    backIsActive: {

    },
    backIsDisabled: {
        display: 'none'
    }
});

export default BasicHeader;