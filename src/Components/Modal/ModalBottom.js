import React from 'react';
import {
    View, StyleSheet
} from 'react-native';

import ButtonDefault from '../../Components/Button/ButtonDefault';

import { Color } from '../../Utils/Colors/Color';

class ModalBottom extends React.Component {
    render() {
        return (
            <View
                style={this.props.isActive ? styles.isActive : styles.noActive}
            >
                <View 
                    style={{
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0,
                        position: 'absolute',
                        backgroundColor: Color.Hitam,
                        opacity: 0.1
                    }}
                />

                <View
                    style={{
                        height: 200,
                        position: 'absolute',
                        bottom: 0,
                        right: 0,
                        left: 0,
                        justifyContent: 'center',
                        paddingHorizontal: 40,
                        backgroundColor: Color.Putih,
                        elevation: 5,
                    }}
                >
                    
                    <ButtonDefault 
                        onPress={this.props.takepicture}
                        title={'Take Picture'}
                        backgroundColor={Color.Hijau}
                    />

                    <ButtonDefault 
                        onPress={this.props.choosegallery}
                        title={'Choose Gallery'}
                        backgroundColor={Color.Hijau}
                    />

                    <ButtonDefault 
                        onPress={this.props.close}
                        title={'Close'}
                        backgroundColor={Color.Merah}
                    />

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    isActive: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    noActive: {
        display: 'none'
    }
})

export default ModalBottom;