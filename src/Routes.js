import React from 'react';
import { 
    Animated
} from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import { fromRight } from 'react-navigation-transitions';

/**
 * Splash Screen
 */
import SplashScreen from './Views/Auth/SplashScreen';

/**
 * Contact
 * ===================================================
 */
import ListContact from './Views/Contact/ListContact';
import DetailContact from './Views/Contact/DetailContact';
import CreateContact from './Views/Contact/CreateContact';


const AppNavigator = createStackNavigator({
    ListContact: ListContact,
    DetailContact: DetailContact,
    CreateContact: CreateContact
}, {
    headerMode: 'none',
    initialRouteName: 'ListContact',
    defaultNavigationOptions: {
        ...TransitionPresets.FadeFromBottomAndroid
    }
});

const AppAuthSwitchNavigator = createSwitchNavigator({
    SplashScreen: SplashScreen,
    AppNavigator: AppNavigator
}, {
    initialRouteName: 'SplashScreen',
    headerMode: 'none'
});

const Routes = createAppContainer(AppAuthSwitchNavigator);
export default Routes;