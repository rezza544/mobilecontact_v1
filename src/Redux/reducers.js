const initialState = {
    /**
     * PROFILE USERS
     */
    firstName: '',
    lastName: '',
    age: '',
    photo: ''
}

const rootReducer = (state = initialState, action) => {
    switch(action.type) {
        case "PROFILE_USERS":
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
                age: action.age,
                photo: action.photo
            }
        case "CLEAR_PROFILE_USERS":
            return {
                ...state,
                firstName: '',
                lastName: '',
                age: '',
                photo: ''
            }

        default:
            return state;
    }
}

export default rootReducer;